window.billReceiveCreateComponent = Vue.extend({
    template: `
     <form name="form" @submit.prevent="submit">
         <label>Vencimento:</label>
         <input type="text" v-model="bill.date_due">
         <br><br>
         <label>Nome:</label>
         <select v-model="bill.name">
         <option v-for="o in names" :value="o">{{ o }}</option> <!--v-bind pode ser subs por :-->
         </select>
         <br><br>
         <label>Valor:</label>
         <input type="text" v-model="bill.value">
         <br><br>
         <label>Recebido?:</label>
         <input type="checkbox" v-model="bill.done">
         <br><br>
         <input type="submit" value="Enviar">
     </form>
 `,
    props: ['bill'],
    data: function () {
        return {
            formType: 'insert',
            names: [
                'Venda Produto 1',
                'Venda Produto 2',
                'Venda Produto 3',
                'Venda Produto 4',
                'Venda Produto 5',
                'Venda Produto 6',
                'Venda Produto 7'
            ],
            bill: {
                date_due: '',
                name: '',
                value: 0,
                done: false
            }
        };
    },
    created: function () {
        if(this.$route.name == 'bill-receive.update'){
            this.formType = 'update';
            this.getBill(this.$route.params.index);
        }
    },
    methods: {
        submit: function () {
            if (this.formType == 'insert') {
                this.$root.$children[0].billsReceive.push(this.bill);
            }
            this.bill = {
                date_due: '',
                name: '',
                value: 0,
                done: false
            };
            this.$router.go({name: 'bill-receive.list'});
        },
        getBill: function (index) {
            var bills = this.$root.$children[0].billsReceive;
            this.bill = bills[index];
        }
    }
});