window.dashboardComponent = Vue.extend({
    template: `
    <h1>{{title}}</h1>
    <h3>Total Contas a Receber: {{receive | currency 'R$ ' 2}}</h3>
    <h3>Total Contas a Pagar: {{pay | currency 'R$ ' 2}}</h3>
    <h3></h3>
`,
    components: {
        'bill-component': billComponent
    },
    data: function () {
        return {
            title: "Dashboard",
        }
    },
    computed: {
        receive: function () {
            var bills = this.$root.$children[0].billsReceive;
            var total = 0;
            for (var i in bills) {
                if (!bills[i].done) {
                    total += bills[i].value;
                }
            }
            return total;
        },
        pay: function () {
            var bills = this.$root.$children[0].billsPay;
            var total = 0;
            for (var i in bills) {
                if (!bills[i].done) {
                    total += bills[i].value;
                }
            }
            return total;
        }
    }
});